#!/usr/bin/python
# Coded With Love By Me
# Mangafox Python Downloader
# Cara pakai : python mangafox.py "judul anime"
# Contoh : python mangafox.py "Nisekoi"

from os       import makedirs
from bs4      import BeautifulSoup
from urllib   import urlretrieve
from urllib2  import urlopen
from os.path  import exists, isfile
import mechanize
import cookielib
import re
import sys
import json

class Crawler:

  def __init__(self, url, chapter):
    self.url = url
    self.html_utama = urlopen(url)
    self.main_soup = BeautifulSoup(self.html_utama.read())
    self.chapter = chapter

  def halaman_crawler(self):
    ambil_url = self.url
    url_split = ambil_url.split('/')
    url_split_cnt = len(url_split)
    split_url_akhir = url_split[-1]

    curr_base_url_split = url_split[0:-1]
    curr_base_url = "/".join(curr_base_url_split)

    while (self.html_utama.code == 200) and (split_url_akhir != ""):

      img_src = self.get_image(self.main_soup)
      dir_name = self.create_dir(ambil_url)
      self.downloadManga(img_src,dir_name)
      link_berikutnya = curr_base_url + '/' + self.ambil_link_berikutnya(ambil_url)
      ambil_url = link_berikutnya
      url_split = ambil_url.split('/')
      split_url_akhir = url_split[-1]
      self.html_utama = urlopen(ambil_url)
      self.main_soup = BeautifulSoup(self.html_utama.read())
      print split_url_akhir

  def get_next_chapter(self, url):
    url_split = url.split('/')
    base_url_split = url_split[0:-1]
    base_url = "/".join(base_url_split)

    next_chapter_num = int(url.split('/')[-2][1:4]) + 1
    next_chapter_link = base_url
    print next_chapter_link

  def ambil_link_berikutnya(self,url):
    halaman_html = urlopen(url)
    bs_berikutnya = BeautifulSoup(halaman_html.read())
    link_berikutnya = bs_berikutnya.find('a', {'class':'next_page'})
    return link_berikutnya.get('href')

  def create_dir(self, url):
    url_split = url.split('/')
    dir_name = url_split[-4] + '_' + url_split[-2]
    return dir_name

  def get_image(self, data):
    viewer = data.find(id='viewer')
    img = viewer.find('img')
    return img.get('src')

  def downloadManga(self,img_src,dir_name):

    letak_direkotri = 'mangadl/' + dir_name
    nama_file = img_src.split('/')[-1]

    if not exists(letak_direkotri):
      makedirs(letak_direkotri)

    if not isfile(letak_direkotri + '/' + nama_file):
      try: 
        br = mechanize.Browser()

        cj = cookielib.LWPCookieJar()
        br.set_cookiejar(cj)

        br.set_handle_equiv(True)
        br.set_handle_gzip(False)
        br.set_handle_redirect(True)
        br.set_handle_referer(True)
        br.set_handle_robots(False)

        br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

        br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:27.0) Gecko/20100101 Firefox/27.0')]
        image_response = br.open_novisit(img_src)

        with open(letak_direkotri + '/' + nama_file, 'wb') as f:
          f.write(image_response.read())
          print('Downloading ' + nama_file + '...')

      except:
        print('Download error!')
    else:
      print('Gambar sudah ada!.')

  def find_chapter(self):
    cari_manga = "Nisekoi" + " " + str(self.chapter)
    hasil = self.main_soup.find_all('a', text=re.compile(cari_manga))
    return hasil[-1].get('href')

def main(argv):
  
  manga_url = 'http://mangafox.me/manga/'
  cari_url = 'http://mangafox.me/ajax/search.php?term='
  cari_manga = argv[0]

  ambil_html = urlopen(cari_url + cari_manga)
  bs = json.loads(ambil_html.read())

  cnt = 1
  for b in bs:
    print str(cnt) + ": " + b[1]
    cnt += 1

  pilihan = raw_input("Massukan nomor =  ")

  choosen_manga = bs[int(pilihan) - 1]

  ambil_link_manga = manga_url + choosen_manga[2]

  pilih_judul_manga = choosen_manga[1]

  chapter = raw_input("Massukan " + choosen_manga[1] + " chapter = ")
  end = raw_input("sampai chapter = ")

  html_utama = urlopen(ambil_link_manga)
  b = BeautifulSoup(html_utama.read())

  try:

    for x in range(int(chapter), int(end)+1):
      print x
      cari_manga = pilih_judul_manga + " " + str(x)
      chapter_link = b.find_all('a', text=re.compile(cari_manga))
      url = chapter_link[-1].get('href')
      print url

      c = Crawler(url,chapter)
      c.halaman_crawler()
  except:
    print "Maaf Error, mungkin Anda kurang amal!"

if __name__ == '__main__':
  main(sys.argv[1:])